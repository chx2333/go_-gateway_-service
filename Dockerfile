# 使用与开发时版本匹配的基础镜像
FROM golang:1.17-alpine AS build

# 设置工作路径
WORKDIR /go/src/app

# 项目整体拷贝
COPY . .
#原始方式：直接镜像内打包编译
RUN export GO111MODULE=auto && export GOPROXY=https://goproxy.cn && go mod tidy

# 编译go源代码 并命名为go_gateway 存放到 /bin/go_gateway
RUN CGO_ENABLED=0 GOOS=linux go build -o ./bin/go_gateway


FROM alpine

WORKDIR /

# 更换镜像源 配置时区
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories && apk update && apk add tzdata

COPY --from=build  /go/src/app/cert_file /cert_file
COPY --from=build /go/src/app/conf /conf
COPY --from=build /go/src/app/bin/go_gateway /go_gateway

# 暴露端口
EXPOSE 8080
EXPOSE 4433
# 执行
ENTRYPOINT ["/go_gateway"]