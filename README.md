# GO_Gateway_Service 网关服务

## 前言

​		该部分为网关项目的服务部分，即处理真正的代理转发请求。目前支持的转发请求有：

🚩 1.http服务代理转发

🚩 2.tcp服务代理转发

🚩 3.https服务代理转发(因证书问题线上无法使用)

🚩 4.grpc服务代理转发

​		网关服务管理前台项目：

🤖Gitee：https://gitee.com/chx2333/go_-gateway_-front

🐱‍🏍Github：

## 🍳HTTP服务代理

​	HTTP服务端口：8080

### 功能

​	1.服务流量统计：统计每个http服务被请求的次数

​	2.服务流量限制：限制某个服务被调用的次数或不同用户可以调用的次数

​	3.服务白名单：只有位于白名单ip列表中的主机可以调用该服务

​	4.服务黑名单：位于黑名单中的主机不能不能调用该服务(白名单和黑名单，只有一个生效，优先级为白名单>黑名单)

​	5.服务请求头转换：可以对下游服务器返回的响应体中的header头字段进行 添加，删除，更新操作

​	6.服务URI的转换：去除标识服务的URI路径

​	7.服务URL重写：依据一定的规则对URL进行重写

​	8.服务反向代理

## 🧇TCP服务代理

​	TCP服务端口：各TCP服务指定的端口

### 功能

## 🥓GRPC服务代理

​	GRPC服务端口：各GRPC服务指定的端口

### 功能

## 开发日记

### <a href="https://www.yuque.com/c_pluto/ed2hxg/hn23w6?singleDoc# 《01.TCP服务解析》">🍕1.TCP服务解析</a>

### <a href="https://www.yuque.com/c_pluto/ed2hxg/yg5318?singleDoc# 《02.正向代理》">🍔2.正向代理</a>

### <a href="https://www.yuque.com/c_pluto/ed2hxg/xt7a1l?singleDoc# 《03.反向代理》">🍟3.反向代理</a>

### <a href="https://www.yuque.com/c_pluto/ed2hxg/qnroki?singleDoc# 《04.ReverseProxy使用——修改response》">🌭4.ReverseProxy修改response</a>

### <a href="https://www.yuque.com/c_pluto/ed2hxg/zccgeg?singleDoc# 《05.负载均衡—随机负载均衡》">🍿5.负载均衡—随机负载均衡</a>

### <a href="https://www.yuque.com/c_pluto/ed2hxg/ndy7c7?singleDoc# 《06.负载均衡—轮询负载均衡》">🧂6.负载均衡—轮询负载均衡</a>

### <a href="https://www.yuque.com/c_pluto/ed2hxg/tryk8f?singleDoc# 《07.负载均衡—加权负载均衡》">🥓7.负载均衡—加权负载均衡</a>

### <a href="https://www.yuque.com/c_pluto/ed2hxg/wl0x9a?singleDoc# 《08.负载均衡—一致性负载均衡》">🥚8.负载均衡—一致性负载均衡</a>

### <a href="https://www.yuque.com/c_pluto/ed2hxg/upn91csnbkr5d6qv?singleDoc# 《09.服务注册—zookeeper》">🍳9.服务注册—zookeeper</a>

### <a href="https://www.yuque.com/c_pluto/ed2hxg/whvucyk3pagernxg?singleDoc# 《10.服务发现—观察者模式》">🧇10.服务发现—观察者模式</a>