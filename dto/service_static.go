package dto

import (
	"go_gateway/public"

	"github.com/gin-gonic/gin"
)

type ServiceStaticInput struct {
	ID int64 `json:"id" form:"id" validate:"required" comment:"服务id"`
}

type ServiceStaticOutput struct {
	Title     string  `json:"title"`
	Today     []int64 `json:"today"`
	Yesterday []int64 `json:"yesterday"`
}

func (params *ServiceStaticInput) BindValidParam(c *gin.Context) error {
	return public.DefaultGetValidParams(c, params)
}
