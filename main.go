package main

import (
	"go_gateway/common/lib"
	"go_gateway/dao"
	"go_gateway/service_http/router"
	tcpService "go_gateway/service_tcp/router"
	"go_gateway/util"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	// 读取配置文件
	err := lib.InitModule("./conf/dev/")
	if err != nil {
		panic(err)
	}
	defer lib.Destroy()

	// 加载数据库中所有服务
	err = dao.ServiceManagerHandler.LoadOnce()
	if err != nil {
		panic(err)
	}
	util.Logo()
	// HTTP代理服务
	go func() {
		httpService.HttpServerRun()
	}()

	// TCP代理服务
	go func() {
		tcpService.TcpServerRun()
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGKILL, syscall.SIGQUIT, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	tcpService.TcpServerStop()
	httpService.HttpServerStop()
}
