package public

import (
	"sync"
	"time"
)

var FlowCounterHandler *FlowCounter

func init() {
	FlowCounterHandler = NewFlowCounter()
}

type FlowCounter struct {
	RedisFlowCountMap   map[string]*RedisFlowCountService
	RedisFlowCountSlice []*RedisFlowCountService
	Locker              sync.RWMutex
}

func NewFlowCounter() *FlowCounter {
	return &FlowCounter{
		RedisFlowCountMap:   map[string]*RedisFlowCountService{},
		RedisFlowCountSlice: []*RedisFlowCountService{},
		Locker:              sync.RWMutex{},
	}
}

func (counter *FlowCounter) GetCounter(serviceName string) (*RedisFlowCountService, error) {
	// 1.判断是否已经在数组中
	for _, item := range counter.RedisFlowCountSlice {
		if item.AppID == serviceName {
			return item, nil
		}
	}

	// 2.创建counter
	newCounter := NewRedisFlowCountService(serviceName, time.Second*1)

	// 3.counter 添加到数组
	counter.RedisFlowCountSlice = append(counter.RedisFlowCountSlice, newCounter)

	counter.Locker.Lock()
	defer counter.Locker.Unlock()
	counter.RedisFlowCountMap[serviceName] = newCounter
	return newCounter, nil
}
