package public

import (
	"golang.org/x/time/rate"
	"sync"
)

var FlowLimiterHandler *FlowLimiter

type FlowLimiter struct {
	FlowLimitMap   map[string]*FlowLimiterItem
	FlowLimitSlice []*FlowLimiterItem
	Locker         sync.RWMutex
}

type FlowLimiterItem struct {
	Limiter     *rate.Limiter
	ServiceName string
}

func init() {
	FlowLimiterHandler = NewFlowLimiter()
}

func NewFlowLimiter() *FlowLimiter {
	return &FlowLimiter{
		FlowLimitMap:   map[string]*FlowLimiterItem{},
		FlowLimitSlice: []*FlowLimiterItem{},
		Locker:         sync.RWMutex{},
	}
}

func (counter *FlowLimiter) GetLimiter(serviceName string, qps float64) (*rate.Limiter, error) {
	for _, item := range counter.FlowLimitSlice {
		if item.ServiceName == serviceName {
			return item.Limiter, nil
		}
	}

	newLimiter := rate.NewLimiter(rate.Limit(qps), int(qps*3))
	item := &FlowLimiterItem{
		Limiter:     newLimiter,
		ServiceName: serviceName,
	}

	counter.FlowLimitSlice = append(counter.FlowLimitSlice, item)

	counter.Locker.Lock()
	defer counter.Locker.Unlock()
	counter.FlowLimitMap[serviceName] = item
	return newLimiter, nil

}
