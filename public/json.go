package public

import "encoding/json"

func obj2JSON(value interface{}) string {
	bytes, _ := json.Marshal(value)
	return string(bytes)
}
