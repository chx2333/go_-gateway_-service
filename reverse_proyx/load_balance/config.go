package load_balance

// LoadBalanceConf
// @Description: 负载均衡配置需要实现的方法
//
type LoadBalanceConf interface {
	Attach(o Observer)
	GetConf() []string
	WatchConf()
	UpdateConf(conf []string)
}

type Observer interface {
	Update()
}
