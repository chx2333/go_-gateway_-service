package load_balance

// LoadBalance
// @Description: LoadBalance接口 不同类型的负载均衡器需要实现的方法
//
type LoadBalance interface {
	Add(...string) error
	Get(string) (string, error)

	//后期服务发现补充
	Update()
}
