package httpMiddleware

import (
	"go_gateway/dao"
	"go_gateway/middleware"

	"github.com/gin-gonic/gin"
)

// HTTPAccessModeMiddleware
// @Description: 处理HTTP的接入方式
// @return gin.HandlerFunc
func HTTPAccessModeMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		service, err := dao.ServiceManagerHandler.HTTPAccessMode(c)
		if err != nil {
			middleware.ResponseError(c, 1001, err)
			c.Abort()
			return
		}
		//fmt.Printf("matched service: %s\n", public.Obj2Json(service))
		// 设置服务信息
		c.Set("service", service)
		c.Next()
	}
}
