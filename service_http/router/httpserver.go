package httpService

import (
	"context"
	"github.com/fatih/color"
	"github.com/gin-gonic/gin"
	"go_gateway/common/lib"
	"go_gateway/middleware"
	"log"
	"net/http"
	"time"
)

var (
	HttpSrvHandler *http.Server
)

// HttpServerRun
// @Description: 启动HTTP代理服务
func HttpServerRun() {
	gin.SetMode(lib.ConfBase.DebugMode)
	r := InitRouter(middleware.RecoveryMiddleware(), middleware.RequestLog())
	HttpSrvHandler = &http.Server{
		Addr:           lib.GetStringConf("proxy.http.addr"),
		Handler:        r,
		ReadTimeout:    time.Duration(lib.GetIntConf("proxy.http.read_timeout")) * time.Second,
		WriteTimeout:   time.Duration(lib.GetIntConf("proxy.http.write_timeout")) * time.Second,
		MaxHeaderBytes: 1 << uint(lib.GetIntConf("proxy.http.max_header_bytes")),
	}
	log.Printf("%s %s %s\n", color.GreenString("[INFO]"), color.BlueString("HTTP_PROXY RUN"), lib.GetStringConf("proxy.http.addr"))
	if err := HttpSrvHandler.ListenAndServe(); err != nil {
		log.Fatalf(" [ERROR] HttpServerRun:%s err:%v\n", lib.GetStringConf("proxy.http.addr"), err)
	}
}

func HttpServerStop() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := HttpSrvHandler.Shutdown(ctx); err != nil {
		log.Fatalf("[ERROR]  HTTP_PROXY_RUN err:%v\n", err)
	}
	log.Printf("%s %s \n", color.GreenString("[INFO]"), color.BlueString("HTTP_PROXY STOP"))
}
