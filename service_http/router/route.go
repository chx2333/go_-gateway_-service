package httpService

import (
	"github.com/gin-gonic/gin"
	httpMiddleware "go_gateway/service_http/middleware"
)

// InitRouter
// @Description: 初始化HTTP代理服务Router
// @param middlewares
// @return *gin.Engine
func InitRouter(middlewares ...gin.HandlerFunc) *gin.Engine {
	router := gin.Default()
	router.Use(middlewares...)
	// 检测接口
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "http is running!!",
		})
	})
	router.Use(httpMiddleware.HTTPAccessModeMiddleware(),
		httpMiddleware.HTTPFlowCountMiddleware(),
		httpMiddleware.HTTPFlowLimitMiddleware(),
		httpMiddleware.HTTPWhiteIpListMiddleware(),
		httpMiddleware.HTTPBlackListMiddleware(),
		httpMiddleware.HTTPHeaderTransferMiddleware(),
		httpMiddleware.HTTPStripUriMiddleware(),
		httpMiddleware.HTTPUrlRewriteMiddleware(),
		httpMiddleware.HTTPReverseProxyMiddleware())
	return router
}
