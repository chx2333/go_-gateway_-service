package tcpServer

import (
	"context"
	"fmt"
	"log"
	"net"
	"runtime"
)

type tcpKeepAliveListener struct {
	*net.TCPListener
}

//todo 思考点：继承方法覆写方法时，只要使用非指针接口
func (ln tcpKeepAliveListener) Accept() (net.Conn, error) {
	tc, err := ln.AcceptTCP()
	if err != nil {
		return nil, err
	}
	return tc, nil
}

type contextKey struct {
	name string
}

func (k *contextKey) String() string {
	return "tcp_proxy context value " + k.name
}

// conn
// @Description: TCP连接结构体
//
type conn struct {
	// server
	// @Description: TCP服务
	//
	server *TcpServer
	// cancelCtx
	// @Description: 连接关闭
	//
	cancelCtx context.CancelFunc

	// rawConn
	// @Description: 原生的conn
	//
	rawConn net.Conn
	// remoteAddr
	// @Description: 连接地址
	//
	remoteAddr string
}

// close
// @Description: 关闭tcp连接
// @receiver c
//
func (c *conn) close() {
	c.rawConn.Close()
}

// serve
// @Description: conn服务
// @receiver c
// @param ctx
//
func (c *conn) serve(ctx context.Context) {
	defer func() {

		if err := recover(); err != nil && err != ErrAbortHandler {
			const size = 64 << 10
			buf := make([]byte, size)
			buf = buf[:runtime.Stack(buf, false)]
			fmt.Printf("tcp: panic serving %v: %v\n%s", c.remoteAddr, err, buf)
		}
		c.close()
	}()
	c.remoteAddr = c.rawConn.RemoteAddr().String()
	ctx = context.WithValue(ctx, LocalAddrContextKey, c.rawConn.LocalAddr())
	if c.server.Handler == nil {
		panic("handler empty")
	}
	log.Println("-----------------")
	log.Println("local Addr: ", ctx.Value(LocalAddrContextKey))
	c.server.Handler.ServeTCP(ctx, c.rawConn)
}
