package tcpServer

import (
	"context"
	"errors"
	"fmt"
	"net"
	"sync"
	"sync/atomic"
	"time"
)

var (
	ErrServerClosed     = errors.New("tcp: Server closed")
	ErrAbortHandler     = errors.New("tcp: abort TCPHandler")
	ServerContextKey    = &contextKey{"tcp-server"}
	LocalAddrContextKey = &contextKey{"local-addr"}
)

type onceCloseListener struct {
	net.Listener
	once     sync.Once
	closeErr error
}

func (oc *onceCloseListener) Close() error {
	oc.once.Do(oc.close)
	return oc.closeErr
}

func (oc *onceCloseListener) close() {
	oc.closeErr = oc.Listener.Close()
}

// TCPHandler
// @Description: 结构体需要是实现方法
//
type TCPHandler interface {
	ServeTCP(ctx context.Context, conn net.Conn)
}

// TcpServer
// @Description: 服务结构体
//
type TcpServer struct {
	Addr    string          // tcp服务器ip地址
	Handler TCPHandler      // TCP处理方法
	err     error           // 错误方法
	BaseCtx context.Context // 上下文

	WriteTimeout     time.Duration // 最大写入时间
	ReadTimeout      time.Duration // 读取时间
	KeepAliveTimeout time.Duration // 保持连接时间

	mu         sync.Mutex // 锁
	inShutdown int32      // 服务是否关闭
	doneChan   chan struct{}
	l          *onceCloseListener // listener监听器
}

// shuttingDown
// @Description: 原子操作读取值 查看是否关闭了
// @receiver s
// @return bool
//
func (s *TcpServer) shuttingDown() bool {
	return atomic.LoadInt32(&s.inShutdown) != 0
}

// ListenAndServe
// @Description: 启动服务监听
// @receiver srv
// @return error
//
func (srv *TcpServer) ListenAndServe() error {
	if srv.shuttingDown() {
		return ErrServerClosed
	}
	if srv.doneChan == nil {
		srv.doneChan = make(chan struct{})
	}
	addr := srv.Addr
	if addr == "" {
		return errors.New("need addr")
	}
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	return srv.Serve(tcpKeepAliveListener{
		ln.(*net.TCPListener)})
}

// Close
// @Description: 服务关闭
// @receiver srv
// @return error
//
func (srv *TcpServer) Close() error {
	atomic.StoreInt32(&srv.inShutdown, 1)
	close(srv.doneChan) //关闭channel
	srv.l.Close()       //执行listener关闭
	return nil
}

// Serve
// @Description: 监听该ip服务
// @receiver srv
// @param l
// @return error
//
func (srv *TcpServer) Serve(l net.Listener) error {
	srv.l = &onceCloseListener{Listener: l}
	defer srv.l.Close() //执行listener关闭
	if srv.BaseCtx == nil {
		srv.BaseCtx = context.Background()
	}
	baseCtx := srv.BaseCtx
	ctx := context.WithValue(baseCtx, ServerContextKey, srv)
	for {
		conn, e := l.Accept()
		if e != nil {
			select {
			case <-srv.getDoneChan():
				return ErrServerClosed
			default:
			}
			fmt.Printf("accept fail, err: %v\n", e)
			continue
		}
		c := srv.newConn(conn)
		go c.serve(ctx)
	}
	return nil
}

func (srv *TcpServer) newConn(rawConn net.Conn) *conn {
	c := &conn{
		server:  srv,
		rawConn: rawConn,
	}
	// 设置参数
	if d := c.server.ReadTimeout; d != 0 {
		c.rawConn.SetReadDeadline(time.Now().Add(d))
	}
	if d := c.server.WriteTimeout; d != 0 {
		c.rawConn.SetWriteDeadline(time.Now().Add(d))
	}
	if d := c.server.KeepAliveTimeout; d != 0 {
		if tcpConn, ok := c.rawConn.(*net.TCPConn); ok {
			tcpConn.SetKeepAlive(true)
			tcpConn.SetKeepAlivePeriod(d)
		}
	}
	return c
}

// getDoneChan
// @Description: 获取channel
// @receiver s
// @return <-chan
//
func (s *TcpServer) getDoneChan() <-chan struct{} {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.doneChan == nil {
		s.doneChan = make(chan struct{})
	}
	return s.doneChan
}

// ListenAndServe
// @Description: 入口方法 生成tcp对应服务
// @param addr
// @param handler
// @return error
//
func ListenAndServe(addr string, handler TCPHandler) error {
	//  创建结构体
	server := &TcpServer{Addr: addr, Handler: handler, doneChan: make(chan struct{})}
	//  嗲用方法
	return server.ListenAndServe()
}
